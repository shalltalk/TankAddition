package ui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import message.Message;

class MyRenderer implements ListCellRenderer<Object> {
	JLabel lbl = new JLabel();

	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		lbl.setText((String) value);
		lbl.setOpaque(false);
		String text = (String) value;
		if (text.contains(Message.DEOX)) {
			lbl.setForeground(new Color(204, 153, 0));
		} else if (text.contains(Message.HEX_FREE)) {
			lbl.setForeground(new Color(51, 204, 51));
		} else {
			lbl.setForeground(Color.BLACK);
		}
		return lbl;
	}
}