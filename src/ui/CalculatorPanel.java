package ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import logic.Facilities;
import logic.Utility;
import message.Message;

public class CalculatorPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTextField concentrationTextField;
	private JTextField solutionLevelTextField;
	private JButton btnCustomize;
	private JTextField facilityLengthTextField;
	private JTextField facilityWidthTextField;
	private JTextField facilityHeightTextField;
	private JTextField targetConcentrationPercentageTextField;
	private JLabel targetConcentrationPercentageLabel;
	private JComboBox<String> facilitProcessComboBox;
	private JTextField xTextField;
	private JTextField xtTextField;
	private JTextField xdiTextField;
	private boolean isCustom = false;
	private Rectangle facilityProcessComboBoxBounds = new Rectangle(236, 11, 254, 24);
	private Rectangle targetConcentrationPercentageTextFieldBounds = new Rectangle(236, 11, 90, 24);
	private Rectangle targetConcentrationPercentageLabelBounds = new Rectangle(336, 11, 18, 24);
	private Rectangle facilityLengthTextFieldBounds = new Rectangle(370, 11, 45, 24);
	private Rectangle facilityWidthTextFieldBounds = new Rectangle(420, 11, 45, 24);
	private Rectangle facilityHeightTextFieldBounds = new Rectangle(470, 11, 45, 24);

	/**
	 * Create the panel.
	 */
	public CalculatorPanel() {
		setBorder(null);
		setLayout(null);

		JLabel lblNewLabel = new JLabel(Message.CURRENT_CONCENTRATION);
		lblNewLabel.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 20));
		lblNewLabel.setBounds(10, 54, 203, 24);
		add(lblNewLabel);

		concentrationTextField = new JTextField();
		concentrationTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					doCalculation();
				}
			}
		});
		concentrationTextField.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 17));
		concentrationTextField.setBounds(236, 54, 191, 24);
		add(concentrationTextField);
		concentrationTextField.setColumns(10);

		JLabel lblPleaseInput1 = new JLabel(Message.SOLUTION_LEVEL1);
		lblPleaseInput1.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 20));
		lblPleaseInput1.setBounds(10, 85, 203, 24);
		add(lblPleaseInput1);

		solutionLevelTextField = new JTextField();
		solutionLevelTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					doCalculation();
				}
			}
		});
		solutionLevelTextField.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 17));
		solutionLevelTextField.setColumns(10);
		solutionLevelTextField.setBounds(236, 96, 191, 24);
		add(solutionLevelTextField);

		JLabel lblX = new JLabel(Message.X);
		lblX.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 20));
		lblX.setBounds(10, 226, 219, 24);
		add(lblX);

		xTextField = new JTextField();
		xTextField.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 17));
		xTextField.setColumns(10);
		xTextField.setBounds(236, 227, 191, 24);
		xTextField.setEditable(false);
		add(xTextField);

		JLabel lblXt = new JLabel(Message.XT);
		lblXt.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 20));
		lblXt.setBounds(10, 191, 219, 24);
		add(lblXt);

		xtTextField = new JTextField();
		xtTextField.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 17));
		xtTextField.setColumns(10);
		xtTextField.setBounds(236, 191, 191, 24);
		xtTextField.setEditable(false);
		add(xtTextField);

		JLabel lblXdi = new JLabel(Message.XDI);
		lblXdi.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 20));
		lblXdi.setBounds(10, 261, 219, 24);
		add(lblXdi);

		xdiTextField = new JTextField();
		xdiTextField.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 17));
		xdiTextField.setColumns(10);
		xdiTextField.setBounds(236, 261, 191, 24);
		xdiTextField.setEditable(false);
		add(xdiTextField);

		JButton calculateButton = new JButton(Message.CALCULATE);
		calculateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doCalculation();
			}
		});
		calculateButton.setBounds(391, 339, 99, 23);
		add(calculateButton);

		JButton clearButton = new JButton(Message.CLEAR);
		clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clear();
			}
		});
		clearButton.setBounds(236, 339, 99, 23);
		add(clearButton);

		JLabel label = new JLabel(Message.PERCENTAGE);
		label.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 17));
		label.setBounds(437, 56, 18, 22);
		add(label);

		JLabel lblInch = new JLabel(Message.INCH);
		lblInch.setFont(new Font("Arial", Font.PLAIN, 17));
		lblInch.setBounds(437, 98, 33, 22);
		add(lblInch);

		JLabel label_1 = new JLabel(Message.GALLON);
		label_1.setFont(new Font("Arial", Font.PLAIN, 17));
		label_1.setBounds(437, 193, 53, 22);
		add(label_1);

		JLabel label_2 = new JLabel(Message.GALLON);
		label_2.setFont(new Font("Arial", Font.PLAIN, 17));
		label_2.setBounds(437, 230, 53, 22);
		add(label_2);

		JLabel label_3 = new JLabel(Message.GALLON);
		label_3.setFont(new Font("Arial", Font.PLAIN, 17));
		label_3.setBounds(437, 263, 53, 22);
		add(label_3);

		JLabel label_4 = new JLabel(Message.XDI_TIP);
		label_4.setFont(new Font("Arial", Font.PLAIN, 15));
		label_4.setBounds(236, 296, 289, 24);
		add(label_4);

		JLabel label_5 = new JLabel("Facility and Process:");
		label_5.setFont(new Font("Arial", Font.PLAIN, 20));
		label_5.setBounds(10, 11, 203, 24);
		add(label_5);

		facilitProcessComboBox = new JComboBox<String>();
		facilitProcessComboBox.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 17));
		facilitProcessComboBox.setModel(new DefaultComboBoxModel<String>(Facilities.getFacilityTankNamesDimensions()));
		facilitProcessComboBox.setBounds(facilityProcessComboBoxBounds);
		facilitProcessComboBox.setRenderer(new MyRenderer());
		add(facilitProcessComboBox);

		JLabel lblPleaseInput2 = new JLabel(Message.SOLUTION_LEVEL2);
		lblPleaseInput2.setFont(new Font("Arial", Font.PLAIN, 20));
		lblPleaseInput2.setBounds(10, 108, 203, 24);
		add(lblPleaseInput2);
		
		btnCustomize = new JButton(Message.SWTICH);
		btnCustomize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				switchFacilityProcess();
			}
		});
		btnCustomize.setBounds(10, 339, 89, 23);
		btnCustomize.setToolTipText(Message.CUSTOM_WARNING);
		add(btnCustomize);
	}


	private void switchFacilityProcess() {
		isCustom = !isCustom;
		if (isCustom) {
			targetConcentrationPercentageTextField = new JTextField();
			targetConcentrationPercentageTextField.setEditable(true);
			targetConcentrationPercentageTextField.setEnabled(true);
			targetConcentrationPercentageTextField.setBounds(targetConcentrationPercentageTextFieldBounds);
			targetConcentrationPercentageTextField.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 17));
			targetConcentrationPercentageTextField.setToolTipText(Message.TARGET_CONCENTRATION);
			targetConcentrationPercentageTextField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						doCalculation();
					}
				}
			});
			
			targetConcentrationPercentageLabel = new JLabel(Message.PERCENTAGE);
			targetConcentrationPercentageLabel.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 17));
			targetConcentrationPercentageLabel.setBounds(targetConcentrationPercentageLabelBounds);

			facilityLengthTextField = new JTextField();
			facilityLengthTextField.setEditable(true);
			facilityLengthTextField.setEnabled(true);
			facilityLengthTextField.setBounds(facilityLengthTextFieldBounds);
			facilityLengthTextField.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 17));
			facilityLengthTextField.setToolTipText(Message.FACILITY_LENGTH);
			facilityLengthTextField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						doCalculation();
					}
				}
			});
			
			facilityWidthTextField = new JTextField();
			facilityWidthTextField.setEditable(true);
			facilityWidthTextField.setEnabled(true);
			facilityWidthTextField.setBounds(facilityWidthTextFieldBounds);
			facilityWidthTextField.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 17));
			facilityWidthTextField.setToolTipText(Message.FACILITY_WIDTH);
			facilityWidthTextField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						doCalculation();
					}
				}
			});
			
			facilityHeightTextField = new JTextField();
			facilityHeightTextField.setEditable(true);
			facilityHeightTextField.setEnabled(true);
			facilityHeightTextField.setBounds(facilityHeightTextFieldBounds);
			facilityHeightTextField.setFont(new Font(Message.FONT_ARIAL, Font.PLAIN, 17));
			facilityHeightTextField.setToolTipText(Message.FACILITY_HEIGHT);
			facilityHeightTextField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						doCalculation();
					}
				}
			});
			
			remove(facilitProcessComboBox);
			add(targetConcentrationPercentageTextField);
			add(targetConcentrationPercentageLabel);
			add(facilityLengthTextField);
			add(facilityWidthTextField);
			add(facilityHeightTextField);
			btnCustomize.setToolTipText(Message.STANDARD_CALCULATION);
		} else {
			add(facilitProcessComboBox);
			remove(targetConcentrationPercentageTextField);
			remove(targetConcentrationPercentageLabel);
			remove(facilityLengthTextField);
			remove(facilityWidthTextField);
			remove(facilityHeightTextField);
			btnCustomize.setToolTipText(Message.CUSTOM_WARNING);
		}
		validate();
        repaint();
	}

	private void doCalculation() {
		if (isCustom) {
			if (verifyCustom()) {
				try {
					Utility.initializeCustomizedTank(targetConcentrationPercentageTextField.getText().trim(), facilityLengthTextField.getText().trim(),
							facilityWidthTextField.getText().trim(), facilityHeightTextField.getText().trim());
					Utility.initializeVariable(concentrationTextField.getText().trim(), solutionLevelTextField.getText().trim());
					Utility.calculateX();
					setOutput();
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null, Message.MESSAGE_INVALID_INPUT, Message.TITLE_INVALID_INPUT,
							JOptionPane.ERROR_MESSAGE);
					return;
				}
			} else {
				JOptionPane.showMessageDialog(null, Message.MESSAGE_INVALID_INPUT, Message.TITLE_INVALID_INPUT,
						JOptionPane.ERROR_MESSAGE);
				return;
			}
		} else {
			if (verify()) {
				String facility = (String) facilitProcessComboBox.getSelectedItem();
				String concentration = concentrationTextField.getText().trim();
				String solutionLevel = solutionLevelTextField.getText().trim();
				try {
					Utility.initializeTank(facility);
					Utility.initializeVariable(concentration, solutionLevel);
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null, Message.MESSAGE_INVALID_INPUT, Message.TITLE_INVALID_INPUT,
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				if (Utility.calculateX()) {
					setOutput();
					Utility.logInputOutput(facility, concentration, solutionLevel, xTextField.getText(),
							xtTextField.getText(), xdiTextField.getText());
				} else {
					// calculation returns negative value, which means input is not practical
					clearOutput();
					JOptionPane.showMessageDialog(null, Message.MESSAGE_INPRACTICAL_INPUT, Message.TITLE_INVALID_INPUT,
							JOptionPane.ERROR_MESSAGE);
				}
			} else {
				clearOutput();
				JOptionPane.showMessageDialog(null, Message.MESSAGE_INVALID_INPUT, Message.TITLE_INVALID_INPUT,
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void clear() {
		concentrationTextField.setText(Message.EMPTY_STRING);
		concentrationTextField.setBackground(Color.WHITE);
		solutionLevelTextField.setText(Message.EMPTY_STRING);
		solutionLevelTextField.setBackground(Color.WHITE);
		facilityLengthTextField.setText(Message.EMPTY_STRING);
		facilityLengthTextField.setBackground(Color.WHITE);
		facilityWidthTextField.setText(Message.EMPTY_STRING);
		facilityWidthTextField.setBackground(Color.WHITE);
		facilityHeightTextField.setText(Message.EMPTY_STRING);
		facilityHeightTextField.setBackground(Color.WHITE);
		targetConcentrationPercentageTextField.setText(Message.EMPTY_STRING);
		targetConcentrationPercentageTextField.setBackground(Color.WHITE);
		facilitProcessComboBox.setSelectedItem(Facilities.SELECT.getValue());
		facilitProcessComboBox.setBackground(Color.WHITE);
		xTextField.setText(Message.EMPTY_STRING);
		xtTextField.setText(Message.EMPTY_STRING);
		xdiTextField.setText(Message.EMPTY_STRING);
	}

	private void clearOutput() {
		xTextField.setText(Message.EMPTY_STRING);
		xtTextField.setText(Message.EMPTY_STRING);
		xdiTextField.setText(Message.EMPTY_STRING);
	}

	private boolean verify() {
		// boolean concentrationSet =
		// ifComponentValueSet(concentrationTextField);
		// boolean concentrationCorrect = ifConcentrationCorrect();
		// if (!(concentrationSet && concentrationCorrect)) {
		// concentrationTextField.setBackground(Color.RED);
		// }
		// boolean solutionLevelSet =
		// ifComponentValueSet(solutionLevelTextField);
		// boolean solutionLevelCorrect = ifSolutionLevelCorrect();
		// if (!(solutionLevelSet && solutionLevelCorrect)) {
		// solutionLevelTextField.setBackground(Color.RED);
		// }

		boolean concentrationCorrect = ifConcentrationCorrect();
		if (!concentrationCorrect) {
			concentrationTextField.setBackground(Color.RED);
			return false;
		} else {
			concentrationTextField.setBackground(Color.WHITE);
		}

		boolean solutionLevelCorrect = ifSolutionLevelCorrect();
		if (!solutionLevelCorrect) {
			solutionLevelTextField.setBackground(Color.RED);
			return false;
		} else {
			solutionLevelTextField.setBackground(Color.WHITE);
		}

		boolean facility = ifFacilityProcessCorrect();
		if (!facility) {
			facilitProcessComboBox.setBackground(Color.RED);
			return false;
		} else {
			facilitProcessComboBox.setBackground(Color.WHITE);
		}
		return true;
	}

	private boolean verifyCustom() {
		if (targetConcentrationPercentageTextField.getText().trim().isEmpty()) {
			targetConcentrationPercentageTextField.setBackground(Color.RED);
			return false;
		} else {
			targetConcentrationPercentageTextField.setBackground(Color.WHITE);
		}
		if (facilityLengthTextField.getText().trim().isEmpty()) {
			facilityLengthTextField.setBackground(Color.RED);
			return false;
		} else {
			facilityLengthTextField.setBackground(Color.WHITE);
		}
		if (facilityWidthTextField.getText().trim().isEmpty()) {
			facilityWidthTextField.setBackground(Color.RED);
			return false;
		} else {
			facilityWidthTextField.setBackground(Color.WHITE);
		}
		if (facilityHeightTextField.getText().trim().isEmpty()) {
			facilityHeightTextField.setBackground(Color.RED);
			return false;
		} else {
			facilityHeightTextField.setBackground(Color.WHITE);
		}
		if (concentrationTextField.getText().trim().isEmpty()) {
			concentrationTextField.setBackground(Color.RED);
			return false;
		} else {
			concentrationTextField.setBackground(Color.WHITE);
		}
		if (solutionLevelTextField.getText().trim().isEmpty()) {
			solutionLevelTextField.setBackground(Color.RED);
			return false;
		} else {
			solutionLevelTextField.setBackground(Color.WHITE);
		}
		return true;
	}

	private boolean ifFacilityProcessCorrect() {
		return !facilitProcessComboBox.getSelectedItem().equals(Facilities.SELECT.getValue());
	}

	private boolean ifConcentrationCorrect() {
		try {
			BigDecimal concentration = new BigDecimal(concentrationTextField.getText().trim());
			return concentration.compareTo(Message.CONCENTRATION_MAX) < 0
					&& concentration.compareTo(Message.CONCENTRATION_MIN) >= 0;
		} catch (NumberFormatException ex) {
			// do nothing
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	private boolean ifSolutionLevelCorrect() {
		try {
			BigDecimal level = new BigDecimal(solutionLevelTextField.getText().trim());
			return level.compareTo(Message.SOLUTION_LEVEL_MIN) >= 0;
		} catch (NumberFormatException ex) {
			// do nothing
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	private void setOutput() {
		concentrationTextField.setBackground(Color.WHITE);
		solutionLevelTextField.setBackground(Color.WHITE);
		facilitProcessComboBox.setBackground(Color.WHITE);
		if (targetConcentrationPercentageTextField != null) {
			targetConcentrationPercentageTextField.setBackground(Color.WHITE);
		}
		if (facilityLengthTextField != null) {
			facilityLengthTextField.setBackground(Color.WHITE);
		}
		if (facilityWidthTextField != null) {
			facilityWidthTextField.setBackground(Color.WHITE);
		}
		if (facilityHeightTextField != null) {
			facilityHeightTextField.setBackground(Color.WHITE);
		}
		xTextField.setText(Utility.getX());
		xtTextField.setText(Utility.getXt());
		xdiTextField.setText(Utility.getXdi());
	}
}
