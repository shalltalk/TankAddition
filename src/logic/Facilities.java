package logic;

import message.Message;

public enum Facilities {
	SELECT(Message.SELECT, 0, 0, 0), 
	AD(Message.ARCADIA_DEOX, 36, 36, 48), 
	OD(Message.ONTARIO_DEOX, 36, 36, 36), 
	SD(Message.SANTA_ANA_DEOX, 36, 36, 48), 
	WD(Message.WARSAW_DEOX, 36, 36, 48), 
	AH(Message.ARCADIA_HEX_FREE, 17, 38, 36), 
	OH(Message.ONTARIO_HEX_FREE, 34, 34, 36), 
	SH(Message.SANTA_ANA_HEX_FREE, 22, 34, 48), 
	WH(Message.WARSAW_HEX_FREE, 23, 35, 48);
	private String value;
	private int length, width, height;

	Facilities(String value, int length, int width, int height) {
		this.value = value;
		this.length = length;
		this.width = width;
		this.height = height;
	}

	public String getValue() {
		return value;
	}

	public int getLength() {
		return length;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public static String[] getFacilityTankNamesDimensions() {
		Facilities[] name = values();
		String[] ret = new String[name.length];
		for (int i = 0; i < ret.length; i++) {
			if (i == 0) {
				ret[i] = name[i].getValue();
			} else {
				StringBuilder sb = new StringBuilder();
				sb.append(name[i].getValue()).append(Message.SPACE).
				append(name[i].getLength()).append(Message.MULTIPLY).
				append(name[i].getWidth()).append(Message.MULTIPLY).
				append(name[i].getHeight());
				ret[i] = sb.toString();
			}
		}
		return ret;
	}

	public static Facilities getFacility(String facilityName) {
		Facilities[] name = values();
		for (Facilities facility : name) {
			if (facilityName.contains(facility.getValue())) {
				return facility;
			}
		}
		return SELECT;
	}
}
