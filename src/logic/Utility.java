package logic;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import message.Message;

public class Utility {
	// values about tank
	private static BigDecimal l;
	private static BigDecimal w;
	private static BigDecimal h;
	// values from input
	private static BigDecimal h1;
	private static BigDecimal c1;
	// values to display
	private static BigDecimal x;
	private static BigDecimal xdi;
	private static BigDecimal xt;
	// value used to calculate
	private static BigDecimal c2;
	private static Logger logger;

	static {
		String username = System.getProperty(Message.USER_NAME);
		System.setProperty(Message.LOG_FOLDER_PROPERTY, Message.C_USER + username + Message.LOG_FOLDER);
		logger = LogManager.getLogger();
	}

	public static void initializeTank(String facilityName) throws NumberFormatException {
		// facilityName can only end with either "Deox" or "Hex-free"
		// "SELECT" has been handled in the GUI.
		if (facilityName.contains(Message.DEOX)) {
			c2 = Message.TARGET_CONCENTRATION_POINT_TWO;
		} else {
			c2 = Message.TARGET_CONCENTRATION_POINT_THREE;
		}
		Facilities facility = Facilities.getFacility(facilityName);
		l = BigDecimal.valueOf(facility.getLength());
		w = BigDecimal.valueOf(facility.getWidth());
		h = BigDecimal.valueOf(facility.getHeight());
	}

	public static void initializeCustomizedTank(String targetConcentration, String length, String width, String height) throws NumberFormatException {
		c2 = new BigDecimal(targetConcentration).divide(Message.HUNDRED);
		l = new BigDecimal(length);
		w = new BigDecimal(width);
		h = new BigDecimal(height);
	}

	public static void initializeVariable(String concentration, String solutionLevel) throws NumberFormatException {
		h1 = h.subtract(new BigDecimal(solutionLevel));
		c1 = (new BigDecimal(concentration)).divide(Message.HUNDRED);
	}

	// Must call initializeTank and initializeVariable before calling this
	public static boolean calculateX() {
		if (h1.compareTo(BigDecimal.ZERO) < 0) {
			return false;
		}
		boolean ret = true;
		// x = (l * w * h1 * (c2 - c1)) / (1d - c2);
		// if (x / (l * w) + h1 <= (h - 3d)) {
		// xt = 0;
		// x = l * w * (h * c2 - 3d * c2 - h1 * c1);
		// xdi = (h - 3d - x / (l * w) - h1) * (l * w);
		// if (xdi < 0 || x < 0) {
		// ret = false;
		// }
		// } else {
		// xdi = 0;
		// xt = l * w * (c2 * (h - 3d) + (1d - c1) * h1 + 3d - h) / (1d - c1);
		// x = l * w * (h - 3d - h1) + xt;
		// if (xt < 0 || x < 0) {
		// ret = false;
		// }
		// }
		x = l.multiply(w).multiply(h1).multiply(c2.subtract(c1)).divide(BigDecimal.ONE.subtract(c2), 6,
				RoundingMode.HALF_UP);
		if (x.divide(l.multiply(w), 6, RoundingMode.HALF_UP).add(h1).compareTo(h.subtract(Message.THREE)) <= 0) {
			xt = BigDecimal.ZERO;
			x = l.multiply(w).multiply(h.multiply(c2).subtract(c2.multiply(Message.THREE)).subtract(h1.multiply(c1)));
			xdi = h.subtract(Message.THREE).subtract(x.divide(l.multiply(w), 6, RoundingMode.HALF_UP)).subtract(h1).multiply(l)
					.multiply(w);
			if (xdi.compareTo(BigDecimal.ZERO) < 0 || x.compareTo(BigDecimal.ZERO) < 0) {
				ret = false;
			}
		} else {
			xdi = BigDecimal.ZERO;
			xt = l.multiply(w).multiply(
					c2.multiply(h.subtract(Message.THREE)).add(h1.multiply(BigDecimal.ONE.subtract(c1))).add(Message.THREE.subtract(h)))
					.divide(BigDecimal.ONE.subtract(c1), 6, RoundingMode.HALF_UP);
			x = l.multiply(w).multiply(h.subtract(Message.THREE).subtract(h1)).add(xt);
			if (xt.compareTo(BigDecimal.ZERO) < 0 || x.compareTo(BigDecimal.ZERO) < 0) {
				ret = false;
			}
		}
		return ret;
	}

	public static String getX() {
		String ret = Message.FORMAT.format(x.divide(Message.CUBIC_INCH_TO_GALLON, 2, RoundingMode.HALF_UP));
		if (ret.equals(Message.POINT_ZERO)) {
			ret = Message.ZERO;
		}
		return ret;
	}

	public static String getXdi() {
		String ret = Message.FORMAT.format(xdi.divide(Message.CUBIC_INCH_TO_GALLON, 2, RoundingMode.HALF_UP));
		if (ret.equals(Message.POINT_ZERO)) {
			ret = Message.ZERO;
		}
		return ret;
	}

	public static String getXt() {
		String ret = Message.FORMAT.format(xt.divide(Message.CUBIC_INCH_TO_GALLON, 2, RoundingMode.HALF_UP));
		if (ret.equals(Message.POINT_ZERO)) {
			ret = Message.ZERO;
		}
		return ret;
	}

	public static void logInputOutput(String facility, String concentration, String solutionLevel, String x, String xt,
			String xdi) {
		StringBuilder sb = new StringBuilder();
		sb.append(facility).append(Message.SPACE).append(concentration).append(Message.SPACE).append(solutionLevel)
				.append(Message.SPACE).append(xt).append(Message.SPACE).append(x).append(Message.SPACE).append(xdi);
		logger.info(sb.toString());
	}
}
