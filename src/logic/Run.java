package logic;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import message.Message;
import ui.CalculatorPanel;

public class Run {

	public static void main(String[] args){
		
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());               	
                	CalculatorPanel panel = new CalculatorPanel();
                	panel.setPreferredSize(new Dimension(535, 370));
                	JFrame frame = new JFrame(Message.TITLE);
                	frame.add(panel);
                	frame.pack();
                	frame.setLocationRelativeTo(null);
                	frame.setVisible(true);
                	frame.setResizable(false);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
	}
}
