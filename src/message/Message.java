package message;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Message {
	// message
	public static final String MESSAGE_INVALID_INPUT = "Input is not valid, please check for errors!";
	public static final String MESSAGE_INPRACTICAL_INPUT = "Input is not practical, please check for errors!";
	public static final String TITLE_INVALID_INPUT = "Invalid Input";
	public static final String MESSAGE_ERROR = "Fatal error, please restart program!";
	public static final String TITLE_ERROR = "Error";
	public static final String CUSTOM_WARNING = "Switch to customized calculations where no limitions are applied on inputs, results might not be practical.";
	public static final String STANDARD_CALCULATION = "Switch to standard calculations with pre-configured tank dimensions and target concentration.";
	// ui
	public static final String TITLE = "Tank Addition Calculator";
	public static final String CURRENT_CONCENTRATION = "Current Concentration:";
	public static final String SOLUTION_LEVEL1 = "Solution Level";
	public static final String SOLUTION_LEVEL2 = "from top of the tank:";
	public static final String FACILITY_PROCESS = "Facility and Process:";
	public static final String FACILITY_LENGTH = "Tank Length";
	public static final String FACILITY_WIDTH = "Tank Width";
	public static final String FACILITY_HEIGHT = "Tank Height";
	public static final String TARGET_CONCENTRATION = "Target Concentration";
	public static final String CALCULATE = "Calculate";
	public static final String CLEAR = "Clear";
	public static final String SWTICH = "Switch";
	public static final String X = "Hex-free/Deox addition: ";
	public static final String XT = "Gallons taken from tank:";
	public static final String XDI = "DI water addition:";
	public static final String XDI_TIP = "Target solution level: 3 inches from the top";
	public static final String PERCENTAGE = "%";
	public static final String INCH = "inch";
	public static final String SPACE = " ";
	public static final String MULTIPLY = "*";
	public static final String GALLON = "gallons";
	// util
	public static final String EMPTY_STRING = "";
	public static final String FONT_ARIAL = "Arial";
	public static final DecimalFormat FORMAT = new DecimalFormat("##.0");// display
	public static final BigDecimal CONCENTRATION_MIN = BigDecimal.valueOf(0d);
	public static final BigDecimal CONCENTRATION_MAX = BigDecimal.valueOf(35d);
	public static final BigDecimal SOLUTION_LEVEL_MIN = BigDecimal.valueOf(0d);
	// facility and process
	public static final String SELECT = "Select";
	public static final String ARCADIA_DEOX = "Arcadia Deox";
	public static final String ARCADIA_HEX_FREE = "Arcadia Hex-free";
	public static final String ONTARIO_DEOX = "Ontario Deox";
	public static final String ONTARIO_HEX_FREE = "Ontario Hex-free";
	public static final String SANTA_ANA_DEOX = "Santa Ana Deox";
	public static final String SANTA_ANA_HEX_FREE = "Santa Ana Hex-free";
	public static final String WARSAW_DEOX = "Warsaw Deox";
	public static final String WARSAW_HEX_FREE = "Warsaw Hex-free";
	public static final String DEOX = "Deox";
	public static final String HEX_FREE = "Hex-free";
	// constants
	public static final BigDecimal CUBIC_INCH_TO_GALLON = BigDecimal.valueOf(231d);
	public static final BigDecimal HUNDRED = BigDecimal.valueOf(100d);
	public static final BigDecimal TARGET_CONCENTRATION_POINT_TWO = BigDecimal.valueOf(0.2d);
	public static final BigDecimal TARGET_CONCENTRATION_POINT_THREE = BigDecimal.valueOf(0.3d);
	public static final BigDecimal THREE = new BigDecimal(3d);
	public static final String POINT_ZERO = ".0";
	public static final String ZERO = "0";
	public static final String DATA_FORMAT = "yyyy/MM/dd HH:mm:ss";
	public static final String USER_NAME = "user.name";
	public static final String LOG_FOLDER_PROPERTY = "TankAdditionLogFilename";
	public static final String C_USER = "C:\\Users\\";
	public static final String LOG_FOLDER = "\\TankAdditionLog";
}
